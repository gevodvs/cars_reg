from django.db import models


# Create your models here.

class Car(models.Model):
    code = models.CharField("code", max_length=2)
    number = models.CharField("number", max_length=4)
    postfix = models.CharField("postfix", max_length=2)
    type = models.CharField("type", max_length=64)
    model = models.CharField("model", max_length=64)
    color = models.CharField("color", max_length=64)
    registrationDate = models.DateField("date of registration", auto_now_add=True)
    status = models.CharField(max_length=32, default="OK")

    class Meta:
        verbose_name = 'Car registration info'
        verbose_name_plural = 'Car registration infos'
