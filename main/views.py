from django.shortcuts import render, redirect
from .models import Car
from .forms import CarForm, CarEditForm


# Create your views here.

def index(request):
    cars = Car.objects.all()
    return render(request,
                  'main/index.html',
                  {'title': 'Main page', 'cars': cars})


def add(request):
    if request.method == 'POST':
        form = CarForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
        else:
            raise Exception("Invalid form")
    else:
        form = CarForm()
        context = {'form': form}
        return render(request, 'main/add.html', context)


def info(request, prefix=None, number=None, postfix=None):
    if prefix and number and postfix:
        car = Car.objects.get(code=prefix, number=number, postfix=postfix)
        print(car)
        cars = [car]
    else:
        cars = []
    return render(request,
                  'main/info.html',
                  {'title': 'Car info', 'cars': cars})


def edit(request, prefix=None, number=None, postfix=None):
    if request.method == 'GET':
        if not (prefix and number and postfix):
            form = CarEditForm()
        else:
            car = Car.objects.get(code=prefix, number=number, postfix=postfix)
            form = CarEditForm(instance=car)
        return render(request, 'main/edit.html', {'form': form})
    else:
        if not (prefix and number and postfix):
            form = CarEditForm(request.POST)
        else:
            car = Car.objects.get(code=prefix, number=number, postfix=postfix)
            form = CarEditForm(request.POST, instance=car)
            if form.is_valid():
                form.save()
    return redirect('index')


def delete(request, prefix=None, number=None, postfix=None):
    if prefix and number and postfix:
        car = Car.objects.get(code=prefix, number=number, postfix=postfix)
        car.delete()
        return redirect('index')

    else:
        raise Exception("Invalid input for delete")
