from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('add', views.add, name='add'),
    path('search', views.add, name='search'),
    path('edit/<str:prefix>/<str:number>/<str:postfix>/', views.edit, name='edit'),
    path('info/<str:prefix>/<str:number>/<str:postfix>/', views.info, name='info'),
    path('delete/<str:prefix>/<str:number>/<str:postfix>/', views.delete, name='delete'),
]
