from .models import Car
from django.forms import ModelForm, Select, TextInput, DateField


class CarForm(ModelForm):
    class Meta:
        CHOICES = (('Option 1', 'Option 1'), ('Option 2', 'Option 2'),)
        model = Car
        fields = ['code',
                  'number',
                  'postfix',
                  'type',
                  'model',
                  'color',
                  'status']
        widgets = {
            'code': Select(choices=(
                ('КА', 'КА'),
                ('КB', 'КB'),
                ('КE', 'КE'),
                ('КH', 'КH'),
                ('КI', 'КI'),
                ('КM', 'КM'),
                ('КO', 'КO'),
                ('КP', 'КP'),
                ('КT', 'КT'),
                ('КX', 'КX'),
                ('HA', 'HA'),
                ('HB', 'HB'),
                ('HE', 'HE'),
                ('HI', 'HI'),
                ('IA', 'IA'),
                ('IB', 'IB'),
            )
            ),
            'number': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Registration number of the auto'}),
            'postfix': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Series of the number'}),
            'type': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Brand of the auto'}),
            'model': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Model of the auto'}),
            'color': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Color of the auto'}),
            'status': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Model of the auto'}),
        }


class CarEditForm(ModelForm):
    class Meta:
        model = Car
        fields = ['type',
                  'model',
                  'color',
                  'status']
        widgets = {
            'type': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Brand of the auto'}),
            'model': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Model of the auto'}),
            'color': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Color of the auto'}),
            'status': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Model of the auto'}),
        }
